# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 17:19:19 2020

@author: Develop
"""
import re
from enum import Enum

class coordinateType(Enum):
    # enum for types of data
    DMS = "DMS" #Sexagesimal degrees, degrees, minutes and seconds 41°24'12.2"N 2°10'26.5"E
    DMM = "DMM" #degrees and decimal minutes 41 24.2028, 2 10.4418
    DD = "DD" #decimal degrees 41.40338, 2.17403

regex = {
    "2NUM":"-?([0-9]|[0-9][0-9])",
    "3NUM":"-?([0-9]|[0-9][0-9]|[0-9][0-9][0-9])",
    "60":"([0-6][0-9]|[0-9])",
    "FLOAT":"(-?[0-9]+\.[0-9]+)",
    "SEP":"[ ]*[ ,;\-][ ]*",
    "NS":"[N|n|S|s]",
    "EW":"[E|e|W|w]"
    }

regexFull = {
    coordinateType.DMS : regex["2NUM"] + "°" + regex["60"] + "'" + regex["FLOAT"] + '"' + regex["NS"] + regex["SEP"] + regex["3NUM"] + "°" + regex["60"] + "'" + regex["FLOAT"] + '"' + regex["EW"],
    coordinateType.DMM : regex["2NUM"] + regex["SEP"] + regex["FLOAT"] + regex["SEP"] + regex["3NUM"] + regex["SEP"] + regex["FLOAT"],
    coordinateType.DD : regex["FLOAT"] + regex["SEP"] + regex["FLOAT"]
    }

class coordinate():
    # coordinate base object
    def __init__(self, coordinates_dict, type_coordinates):
        lat = coordinates_dict["lat"]
        lon = coordinates_dict["lon"]
        coord_type = type_coordinates
    
class coordinateTransformer():
    #class selector of transformations, saves coordinates, transformation results
    pass

def coordinateParser(data_to_parse, type_coordinates):
    #default
    thisregex = regexFull[type_coordinates]
    if type_coordinates == coordinateType.DMS: #41°24'12.2"N 2°10'26.5"E
        pass
    if type_coordinates == coordinateType.DMM: #41 24.2028, 2 10.4418
        pass
    if type_coordinates == coordinateType.DD: #41.40338, 2.17403
        pass